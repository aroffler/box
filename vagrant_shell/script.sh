#!/bin/bash

privileged: true
set -x
apt-get update

#################
# install packages to allow apt to use a repository over HTTPS
apt-get -y install \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
  sudo apt-key add -

#################
# install docker-ce
add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
apt-get update
apt-get -y install docker-ce
usermod -aG docker vagrant
systemctl enable docker
docker --version

#################
# install docker-compose
curl -L \
  "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" \
  -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version

#################
# install bash completion for docker-compose
curl -L \
  https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose \
  -o /etc/bash_completion.d/docker-compose

#################
# install docker-machine
curl -L \
  https://github.com/docker/machine/releases/download/v0.10.0/docker-machine-`uname -s`-`uname -m` \
  > /usr/local/bin/docker-machine
chmod +x /usr/local/bin/docker-machine

#################
# install Unzip
apt-get install -y unzip
unzip -v | grep "UnZip 6"

#################
# Setup Python and Pip
sudo apt-get install -y python3-distutils
apt-get install -y python-pip
apt-get install -y python3-pip
export PATH="~/.local/bin:$PATH"

#################
# install AWS CLI
pip3 install awscli --upgrade
aws --version

#################
# install git
sudo apt-get install git-all -y

#################
# install linuxbrew
# LOL not.....

# #################
# # install sam-cli
pip3 install aws-sam-cli
sam --version


#################
# install terraform latest
cd /tmp/
wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
unzip -o terraform_0.12.24_linux_amd64.zip
sudo mv terraform /usr/local/bin/
terraform --version

#################
# install terraform 11 latest and set binary to terraform11
wget https://releases.hashicorp.com/terraform/0.11.3/terraform_0.11.3_linux_amd64.zip
unzip -o terraform_0.11.3_linux_amd64.zip
sudo mv terraform /usr/local/bin/terraform11
terraform11 --version

# #################
# # install AXE dependencies
apt-get install -y \
libkrb5-dev \
python-gssapi \
python-kerberos \
python-requests-kerberos \
python-simplejson \
bash-completion \
virtualenv \
jq \
graphviz

# ##################
# # install AXE and configure
test -e axe/ || git clone https://bitbucket.org/kxseven/axe.git
cd axe
AXE_ROOT=$(pwd)
export AXE_ROOT

cd $AXE_ROOT
virtualenv local/python

source $AXE_ROOT/etc/axerc
pip install -r requirements/requirements.python2

cat >> ~/.bashrc <<-EOF
# AXE Environment
AXE_ROOT=$AXE_ROOT
PATH=\$PATH:\$AXE_ROOT/bin
export AXE_ROOT PATH
# Useful alias
alias goaxe='cd \$AXE_ROOT; . etc/axerc'
EOF

goaxe

#################
# remove some bloat
apt-get -y autoremove

# #################
# # set custom prompt and functions/aliases in .bashrc for users vagrant and root
# cat /vagrant/bashrc-mod.txt >> /home/vagrant/.bashrc
# cat /vagrant/bashrc-mod.txt >> /root/.bashrc

#################
# set host name
hostname "box"
echo "box" > /etc/hostname
sed -i 's/localhost/localhost       box/' /etc/hosts