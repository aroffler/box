# Box
> test prototypes for integrated provisoner automation over gitlab cicd

Docker is stored here: https://hub.docker.com/r/espressoknight/box

## Packages needed
- packer
- ansible
- vagrant
- docker

## Dependancies
- python
- pip
- git

> Gitlab Runner for any automated CICD tasks. I installed the above packages to the runner and use it for automated deployents.

## packer and docker with ansible
- Packer is used to build a docker image (debian).
- Packer then uses ansible provisoner to remote execute packages and scripts inside the container.
- Packer pushes the new container image to docker.io with tag.


## vagrant and shell script
use `config.vm.provision "shell", path: "script.sh"` to execute a longer shell inline execution


## vagrant and ansible


## packer and vagrant with ansible

